﻿using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MicroService
{
    class Program
    {
        static void Main(string[] args)
        {
            var log = new LoggerConfiguration().WriteTo.Console().CreateLogger();
            Log.Logger = log;

            Task task = new Task(() =>
            {
                while (true)
                {
                    var producer = new Producer();
                    var service = "Microservice Hello World";
                    var timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                    var id = Guid.NewGuid().ToString("N");

                    producer.Produce(String.Concat("Service: ", service, ", Hello World, Timestamp: ", timeStamp, ", Id: ", id));
                    Thread.Sleep(5000);
                }
            });
            task.Start();


            Console.WriteLine("Digite 'q' para monitorar a fila ou digite uma mensagem.");
            var key = Console.ReadLine();

            if (key == "q")
            {
                var consumer = new Consumer();
                consumer.Listen(Console.WriteLine);
            }
            else
            {
                var producer = new Producer();
                producer.Produce(key);
            }
        }
    }
}