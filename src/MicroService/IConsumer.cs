﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService
{
    interface IConsumer
    {
        void Listen(Action<string> message);
    }
}
