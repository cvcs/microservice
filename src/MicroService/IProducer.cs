﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroService
{
    interface IProducer
    {
        void Produce(string message);
    }
}
