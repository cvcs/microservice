﻿using Confluent.Kafka;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MicroService
{
    public class Consumer : IConsumer
    {
        public void Listen(Action<string> message)
        {
            var config = new ConsumerConfig
            {
                BootstrapServers = "localhost:9092",
                GroupId = "Microservice Hello World",
                AutoOffsetReset = AutoOffsetReset.Earliest
            };

            using (var consumer = new ConsumerBuilder<Ignore, string>(config).Build())
            {
                consumer.Subscribe("microservice_topic");
                TimeSpan ts = new TimeSpan(3000);
                while (true)
                {
                    var consumerResult = consumer.Consume(ts);

                    if (consumerResult != null)
                        Log.Information($"Mensagem: {consumerResult.Value}");
                }
            }
        }
    }
}